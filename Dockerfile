FROM golang:1.13-alpine as builder

RUN apk --no-cache add build-base git

WORKDIR /go/src/gitlab-runner-docker-cleanup
COPY . .

RUN go get -v -d
RUN go install

FROM alpine

COPY --from=builder /go/bin/gitlab-runner-docker-cleanup /usr/local/bin/

ENTRYPOINT ["gitlab-runner-docker-cleanup"]
